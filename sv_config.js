SiteVibes.config = {
    categoryNameOverrides: {            //object used to replace sanitized category values with desired values
                                        //or eassily allow for compensating for one offs
    },

    dependencyTimeout: 10000,

    tmiID: 'tmsvi100153',

    apiID: '7d708',       //the client id used to get data for the trending wall
                          //see : http://dev.deliverframework.net:7123/display/wiki/Display+Scripts

    apiUrl: '',            //auto populated at start up with apiID
    
    clientName: 'artbeads',  //used to auto generate the assets url, folder name on prod server
    clientHomePage: 'http://www.artbeads.com', //full link to hp
    clientTwitterName:'',

    roleDependencies:{
        'product_detail'  : [{'link':'productDetailStyles'}, {'script':'productDetailLoadScripts'}],  //there probably shouldn't ever be any for this one
        'trending_wall'   : [{'link':'trendingWallLoadStyles'}, {'script':'trendingWallLoadScripts'}],
        'trending_widget' : [{'link':'trendingWidgetLoadStyles'}, {'script':'trendingWidgetLoadScripts'}],
        'banner'          : [{'link':'bannerLoadStyles'}, {'script':'bannerLoadScripts'}],
        'home_page_widget': [{'link':'homePageWidgetLoadStyles'}, {'script':'homePageWidgetLoadScripts'}],
        'search_result'   : [{'script':'searchResultLoadScripts'}]
    },

    productDetailStyles:[
        'css/productDetail.css'
    ],

    productDetailLoadScripts:[
        'js/roles/product_detail.js'
    ],

    trendingWidgetLoadStyles:[
        'css/trendingWidgetStyle.css'
    ],

    trendingWidgetLoadScripts:[
        'js/roles/trending_widget.js'
    ],

    bannerLoadStyles:[              //default dependencies for the banners
        "css/banner-grid-tablet.css",
        "css/banner.css",
        "http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css",
    ],

    bannerLoadScripts: [
        'js/roles/banner.js'
    ],

    trendingWallLoadStyles:[
        "css/trendingWallStyle.css",
        'http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css',
    ],
    trendingWallLoadScripts: [
        'js/roles/trending_wall.js',
    ],

    searchResultLoadScripts: [
        'js/roles/search_result.js',
    ],

    homePageWidgetLoadStyles:[
        'css/homePageWidget.css?_='+Date.now(),
        'css/3x3-layout/3x3-style.css?_='+Date.now(),
        //'css/normalize.css?_='+Date.now(),
        'http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css',
    ],

    homePageWidgetLoadScripts:[
        'js/roles/homePageWidget.js?_'+Date.now()
    ],

    roleDetectors:[
        {
            role: 'product_detail',
            selector:'#product_detail',
        },
        {
            role: 'trending_wall',
            selector:'#sv-container',
            onDetect: function(svRef){  //reference to SiteVibes object
                svRef.dataContainer = this.selector;
            }
        },
        {
            role: 'trending_widget',
            urlRegex:[/category-page\.html/]
        },
        {
            role: 'banner',
            urlRegex:/category-page-banners\.html/
        },
        {
            role: 'search_result',
            urlRegex:/searchPage/,
        },
        {
            role: 'home_page_widget',
            urlRegex:/home\.html$/,
        },
    ],

    trendingWidgetAfter: '#results', //add the trending widget after this element

    bannerAfter: '#results',        //which element to put the banner in
    
    trendingWidgetTabs:         //key value pairs for trending widget tabs.  
    {                           //value is whats displayed, key is whats used to sort
        'tf=Just%20Added%20to%20Cart':'Just Added To Cart',
        'tf=Hot%20Now&vn=0':'Viewing Now'
    },


    trendingWidgetLoadOpen: false,      //set this to true to initially load widget
                                        //open

    trendingWidgetSide: 'right',         //what side the trending widget should sit on
    
    trendingWidgetInitBounce:true,      //should widget initially bounce open with
                                        //a programattically triggered click

    trendingWidgetBounceOpen: false,   //always bounce open

    trendingWidgetHardLimit: 21,        //a hard limit the widget will stop grabbing
                                       //more products at once reached

    trendingWidgetVerticalScroll: false,   //if true products scroll with more buttons
                                          //rather than jump

    bannerHardLimit:24,                //number of banner results to fetch

    bannerMinLimit:8,                  //minimum results required

    bannerPerPage: 4,                  //number of items rendered on a banner page

    /* stuff related to trending page */
    icon_color: "#004693",
    button_color: "#004693",

    tagName:'',           //current tag sort on trending page
    numItems:0,           //total number of items counter
    preselect:'',         //for tags system, holdover from earlier version may need refactor

    showScrollToTop: true,            //give users something to click that scrolls
                                      //them to the top of the trending wall
    
    //html string that will be used for scroll to top button if showScrollToTop
    //equals true 
    scrollToTopString:  '<img src="'+SiteVibes.assetsUrl+'img/arrowup-img.jpg" alt="Scroll Up"/>',

    displayIcon: {
            "Just Added to Cart": "fa fa-shopping-cart fa-2x",
            "Popular this Month": "fa fa-calendar fa-2x",
            "Popular this Week": "fa fa-star fa-2x",
            "Popular Today": "fa fa-clock-o fa-2x",
            "Hot Now": "fa fa-line-chart fa-2x",
    },

    displayName: {
            "Popular this Month": "Monthly trend",
            "Popular this Week": "Trending this week",
            "Popular Today": "24 hour trend",
            "Hot Now": "Trending this hour",
            "Just Added to Cart": "Just Added to Cart"
    },

    injectSocialButtons: true,                      //if true we will inject the configured
                                                    //social share buttons

    injectSocialButtonsAfter: '#product_name',

    socialShareButtons:[                             //the buttons that will be injected
        'facebook',
        'twitter',
        'pinterest',
        'yahooMail',
        'gmail'
    ],
    allOff:false,
    bannersOff:true,
    homeWidgetOff:true,
    trendingWidgetOff:true,
    searchResultOff:false,
    trendingWallOff:true,
    productDetailOff:true,
}